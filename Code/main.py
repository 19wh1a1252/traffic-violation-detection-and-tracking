import cv2
import numpy as np
from time import sleep
width = 80    #minimum rectangle width
height = 80   #minimum rectangle height
offset=4      #Allowable error btw pixel  
position=550    #line position
delay= 60       #FPS (frames per second)
detect = []
cars= 0

	
def get_centre(x, y, w, h):
    x1 = int(w / 2)
    y1 = int(h / 2)
    cx = x + x1
    cy = y + y1
    return cx,cy

cap = cv2.VideoCapture("Videos/video5.mp4")
subtraction = cv2.createBackgroundSubtractorMOG2()

while True:
    right, frame1 = cap.read()
    tempo = float(1/delay)
    sleep(tempo) 
    grey = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(grey,(3,3),5)
    img_sub = subtraction.apply(blur)
    dilat = cv2.dilate(img_sub,np.ones((5,5)))
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    dilated = cv2.morphologyEx (dilat, cv2. MORPH_CLOSE , kernel)
    dilated = cv2.morphologyEx (dilated, cv2. MORPH_CLOSE , kernel)
    outline,h=cv2.findContours(dilated,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    cv2.line(frame1, (25, position), (1200, position), (255,127,0), 3) 
    for(i,c) in enumerate(outline):
        (x,y,w,h) = cv2.boundingRect(c)
        valid_outline = (w >= width) and (h >= height)
        if not valid_outline:
            continue

        cv2.rectangle(frame1,(x,y),(x+w,y+h),(0,255,0),2)        
        centre = get_centre(x, y, w, h)
        detect.append(centre)
        cv2.circle(frame1, centre, 4, (0, 0,255), -1)

        for (x,y) in detect:
            if y<(position+offset) and y>(position-offset):
                #cars+=1
                cv2.line(frame1, (25, position), (1200, position), (0,127,255), 3)  
                #detect.remove((x,y))
                #print("car is detected : "+str(cars))        
       
        #cv2.putText(frame1, "VEHICLE COUNT : "+str(cars), (450, 70), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255),5)
    cv2.imshow("Traffic Violation Detection and Tracking" , frame1)
  #  cv2.imshow("Detector",dilated)

    if cv2.waitKey(1) == 27:
        break
    
cv2.destroyAllWindows()
cap.release()
