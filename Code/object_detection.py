import cv2
import numpy as np
from time import sleep

width =  80    #minimum rectangle width
height = 80  #minimum rectangle height 
position=550    #line position
delay= 60       #FPS (frames per second)

cap = cv2.VideoCapture("C:\\Users\\PREETHI\\Downloads\\license_plates\\video1.mp4")
subtraction = cv2.createBackgroundSubtractorMOG2()

while True:
    right, frame1 = cap.read()
    tempo = float(1/delay)
    sleep(tempo) 
    grey = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(grey,(3,3),5)
    img_sub = subtraction.apply(blur)
    dilat = cv2.dilate(img_sub,np.ones((5,5)))
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    dilated = cv2.morphologyEx (dilat, cv2. MORPH_CLOSE , kernel)
    outline,h=cv2.findContours(dilated,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    cv2.line(frame1, (25, position), (1200, position), (255,127,0), 3) 
    for(i,c) in enumerate(outline):
        (x,y,w,h) = cv2.boundingRect(c)
        valid_outline = (w >= width) and (h >= height)
        if not valid_outline:
            continue

        cv2.rectangle(frame1,(x,y),(x+w,y+h),(0,255,0),2)        
       
    cv2.imshow("Video Original" , frame1)

    if cv2.waitKey(0) == 27:
        break
    
cv2.destroyAllWindows()
cap.release()
