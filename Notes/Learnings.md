## Open CV:
    OpenCV is a cross-platform library using which we can develop real-time computer vision applications.   
    It mainly focuses on image processing,video capture and analysis including features like face detection and object detection. 
   ## link --  https://docs.opencv.org/master/d6/d00/tutorialpyroot.html. 
## YOLOv3:
   YOLOv3 is the latest variant of a popular object detection algorithm YOLO – You Only Look Once.   
   The published model recognizes 80 different objects in images and videos, but most importantly it is super fast and nearly as accurate as Single Shot MultiBox (SSD).  
   YOLOv3 (You Only Look Once, Version 3) is a real-time object detection algorithm that identifies specific objects in videos, live feeds, or images.  
   YOLO is implemented using the Keras or OpenCV deep learning libraries.  
   YOLO is a Convolutional Neural Network (CNN) for doing object detection.   
   CNN’s are classifier-based systems that can process input images as structured arrays of data and identify patterns between them.  
   YOLO has the advantage of being much faster than other networks and still maintains accuracy.  
## GUI:
    The Graphical User Interface (GUI) makes the system interactive for the user to use.   
    User can monitor the traffic footage and get the alert of violation with the detected bounding box of vehicle.   
    User can take further action using the GUI.  
## TensorFlow:
   TensorFlow is an end-to-end open source platform for machine learning. It has a comprehensive, flexible ecosystem of tools, libraries  and community resources that lets researchers push the state-of-the-art in ML and developers easily build and deploy ML powered applications.

## Keras:
   Tensor Flow Interface.
## PyQt5
   PyQt is a Python binding for Qt, which is a set of C++ libraries and development tools that 
   include platform-independent abstractions for Graphical User Interfaces (GUI), as well as networking, threads,regular expressions,
   SQL databases, SVG, OpenGL, XML, and many other powerful features.
   PyQt5 is one of the most used modules in building GUI apps in Python, and that's due to its simplicity as you will see. 
   Another great feature that encourages developers to use PyQt5 is the PyQt5 designer, 
   which makes it so easy to develop complex GUI apps in a short time. 
   You just drag your widgets to build your form.
## QDarkStyle
   qdarkstyle-provides a function to load the stylesheets transparently with the right resource file
## imutils
   A series of convenience functions to make basic image processing functions such as translation, rotation, resizing, 
   skeletonization, displaying Matplotlib images, sorting contours, detecting edges, 
   and much more easier with OpenCV and both Python 2.7 and Python 3.
## pandas
   Data visualization.
## NumPy
  Numpy stores the contours in an array   
## pyimageSearch 
  https://www.pyimagesearch.com/2014/04/21/building-pokedex-python-finding-game-boy-screen-step-4-6/
