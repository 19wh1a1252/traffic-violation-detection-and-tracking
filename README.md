# Project Name : Number Plate Detection and Tracking

# Description:
The goal of the Project is to  make it easy for the traffic police department to monitor the traffic and take action against the violated vehicle owner in a fast efficient way.Tracking the vehicle and their activities accurately is the main priority of the system.

# Number Plate Detection and Extraction involves a series Steps:
1. Taking an image of a vehicle from Local Harddisk.
2. Localizing the number Plate of that vehicle.
3. Extracting the Number plate data using OCR(Optical Character Recognition).
4. Identifying number plate information using NLC sites like(Parivaahan).

# Team Members:
GUJJARLAPUDI SPANDANA : 19WH1A1252 : IT   
POOSALA PREETHI : 19WH1A1231 : IT   
SUKHAVASI NIKHITHA : 19WH1A0516 : CSE    
B LAKSHMI TEJASWINI : 19WH1A0206 : EEE   
CH JENNIFER : 19WH1A0462 : ECE   
MITTAPELLY ARADYA : 19WH1A0525 : CSE

# TechStack
-  Python               - Coding Language.
-  Visual Studio Code   - Coding Platform.
-  GitLab               - Version Control.
-  imutils              - Image Processing.
-  pytesseract          - Optical Character Recognition.

